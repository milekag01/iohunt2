var express          = require("express");
var bodyParser       = require("body-parser");
var cookieParser     = require('cookie-parser');
var session          = require("express-session");
var passport         = require("passport");
var flash            = require('connect-flash');
var LocalStrategy    = require('passport-local').Strategy;
var expressValidator = require("express-validator");
var mysql            = require("mysql");
var app=express();

const db=mysql.createConnection({
  host:'localhost',
  user:'root',
  password:'',
  port:3306,
  database:'iohuntsql3'
});

db.connect((err)=>{
  if(err){
      console.log("error while connecting to db");
  }else{
  console.log("mysql connected");
  }
});

var indexroutes = require("./routes/routes");
var userroutes = require("./routes/user");
// var { DateTime } = require('luxon');



// require("./config/passport");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
// app.use(expressValidator());

app.set("view engine","ejs");
app.use(express.static("public"));
app.use(cookieParser());

app.use(session({
    secret:'13 reasons why',
    resave: false,
    saveUninitialized: false,
    cookie: {maxAge: 2*60*60*1000}//store session for 12 hrs 
}));
app.use(flash()); //it needs to initialize session first
app.use(passport.initialize());
app.use(passport.session());

//to pass login to be available in all views
//so that we can show or hide buttons accordingily
app.use(function(req,res,next){
    res.locals.login = req.isAuthenticated();
    res.locals.session = req.session; //making sessions available to all routes without passing to individual routes
    res.locals.error = req.flash('error');
    res.locals.success = req.flash('success');
    next();
});

// Express Validator
app.use(expressValidator({
  errorFormatter: function(param, msg, value) {
      var namespace = param.split('.')
      , root    = namespace.shift()
      , formParam = root;

    while(namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
    return {
      param : formParam,
      msg   : msg,
      value : value
    };
  }
}));

// //global variable
// app.use(function (req, res, next) {
//   res.locals.success_msg = req.flash('success_msg');
//   res.locals.error_msg = req.flash('error_msg');
//   res.locals.error = req.flash('error');
//   res.locals.user = req.user || null;
//   next();
// });


app.use('/iohunt18/user',userroutes);
app.use(indexroutes);

app.listen(8080,function(){
    console.log("server started");
});
