const express = require("express");
const router = express.Router();
const ejs = require('ejs');
const { DateTime } = require('luxon');
const mysql = require('mysql');

const db=mysql.createConnection({
	host:'localhost',
	user:'root',
	password:'qwerty123',
	port:3306,
	database:'iohuntsql3'
});

db.connect((err) => {
	if(err){
		console.log("error while connecting to db");
	}else{
		console.log('mysql connected');
	}
});

router.get("/",function(req,res){
    res.redirect("/iohunt18");
});

router.get("/iohunt18",function(req,res){
    res.render("rules");
});

//router.get("/iohunt18/user/profile",ensureAuthenticated,function(req,res,next){
//    res.render("./user/profile");
//})
 router.get("/iohunt18/user/profile",ensureAuthenticated,function(req,res,next){
     res.render("./user/video");
 })

// router.get("/iohunt18/user/profile",ensureAuthenticated,function(req,res,next){
//     res.redirect("/iohunt18/demizAmend");
// })

// Q-1
router.get('/iohunt18/demizAmend', ensureAuthenticated,function(req,res){
	res.redirect('/iohunt18/VNz6bVCooSc2XXWtKeG8');
});
// q-2
router.get('/iohunt18/1/mentos', ensureAuthenticated,function(req,res){
	res.redirect('/iohunt18/A43Kdh5CpnG1lhq7K9cp');
});
// q-3
router.get('/iohunt18/2/agathachristie', ensureAuthenticated,function(req,res){
	res.redirect('/iohunt18/iy6d4XdrydMXIAbETSPl');
});
// q-4
router.get('/iohunt18/3/axiomlalmatia', ensureAuthenticated,function(req,res){
	res.redirect('/iohunt18/Yq0qiEDRAiWQig3ju4lY');
});
// q-5
router.get('/iohunt18/4/hiroshigamo', ensureAuthenticated,function(req,res){
	res.redirect('/iohunt18/68eMloiWF5rSL34KYXwJ');
});
// q-6
router.get('/iohunt18/5/pulitzer', ensureAuthenticated,function(req,res){
	res.redirect('/iohunt18/Cg9wbuiNM6czTuyqlitQ');
});
// q-7
router.get('/iohunt18/6/twinfilms', ensureAuthenticated,function(req,res){
	res.redirect('/iohunt18/H6j3cnGxCogzsyz00PYN');
});
// q-8
router.get('/iohunt18/7/luxo', ensureAuthenticated,function(req,res){
	res.redirect('/iohunt18/j37ONBQ7Z3sw1ze5dgMJ');
});
// q-9
router.get('/iohunt18/8/hello', ensureAuthenticated,function(req,res){
	res.redirect('/iohunt18/XHSXTLbWsoe40eaY1b9p');
});
// q-10
router.get('/iohunt18/9/twofour', ensureAuthenticated,function(req,res){
	res.redirect('/iohunt18/dPub9kJXHsRhiVIVgxzy');
});
// q-11
router.get('/iohunt18/10/lynx', ensureAuthenticated,function(req,res){
	res.redirect('/iohunt18/LawKNUyvi3eQkRqdySpi');
});
// q-12
router.get('/iohunt18/11/aarav', ensureAuthenticated,function(req,res){
	res.redirect('/iohunt18/YVEMRt7IjsvesOdwpspF');
});
// q-13
router.get('/iohunt18/12/saadathasanmanto', ensureAuthenticated,function(req,res){
	res.redirect('/iohunt18/idsQm5HGapxfpRcSA5WI');
});
// q-14
router.get('/iohunt18/13/judasiscariot', ensureAuthenticated,function(req,res){
	res.redirect('/iohunt18/xbsoKFP3lBJjKlEj59mB');
});
// q-15
router.get('/iohunt18/14/hasbro', ensureAuthenticated,function(req,res){
	res.redirect('/iohunt18/QzlOShiD94m2DBcQewn7');
});
// q-16
router.get('/iohunt18/15/harlemglobetrotters', ensureAuthenticated,function(req,res){
	res.redirect('/iohunt18/BK4tmKzojv4P8uKbJ7dm');
});
// q-17
router.get('/iohunt18/16/philosophy', ensureAuthenticated,function(req,res){
	res.redirect('/iohunt18/B4K2oyjyKfkjfiLypamW');
});
// q-18
router.get('/iohunt18/17/roaldamundsen', ensureAuthenticated,function(req,res){
	res.redirect('/iohunt18/vDxRYDdHtNPj8Tk9DjlM');
});
// q-19
router.get('/iohunt18/18/chandigarh', ensureAuthenticated,function(req,res){
	res.redirect('/iohunt18/jWkT9AubnTLZIOYzfcVb');
});
// q-20
router.get('/iohunt18/19/tokillamockingbird', ensureAuthenticated,function(req,res){
	res.redirect('/iohunt18/2eGxoYMPaCmk1Jy1tgDY');
});
// q-21
router.get('/iohunt18/20/firefox', ensureAuthenticated,function(req,res){
	res.redirect('/iohunt18/hA0wKE0G4wgrGA6evRV3');
});
// winner
router.get('/iohunt18/21/threethreezeroone', ensureAuthenticated,function(req,res){
	res.redirect('/iohunt18/qiEDRAiWQig3ju4');
});

router.get('/iohunt18/:qno/*',ensureAuthenticated,function(req,res){
	res.render('error');
})

router.get("/iohunt18/qiEDRAiWQig3ju4",ensureAuthenticated,function(req,res,next){
	res.render('winner');

	var sql3=`UPDATE result SET ? WHERE q_no=21 AND teamname="${req.user.username}"`;
	var resultstr ={
		teamname: req.user.username,
		q_no:  22,
		time: DateTime.local().setZone('Asia/Kolkata')
	}
	db.query(sql3,resultstr,(err4,result4) => {
		if(err4){
			console.log("not tracking in result");
		}else{
			console.log(req.user.username);
			console.log("tracked in result");
		}
	});
})

//fetching question from db
router.get('/iohunt18/:queid', ensureAuthenticated,function(req,response){
    let sql = `SELECT id,question FROM questionbank WHERE quecode="${req.params.queid}"`;
    // console.log(typeof(sql));
    let query = db.query(sql,(err,result) => {
        if(err || !result.length){
            console.log("question not found");
            response.render('error');
            // console.log("queryid: "+ req.params.queid);
        }else{
            console.log(result[0]);
			response.render('question',{foundque : result[0], teamname: req.user.username});
			//TRACKER

            // let tracker ={
            //     teamname: req.user.username,
            //     q_no:  result[0].id,
			// 	time: DateTime.local().setZone('Asia/Kolkata')
			// }
			
            // let sql2='INSERT INTO tracker SET ?';
            // db.query(sql2,tracker,(err2,result2) => {
            //     if(err2){
            //         console.log("not tracking");
            //     }else{
			// 		console.log(req.user.username);
            //         console.log("tracked");
            //     }
			// });
			
			//result-----------------
			var count=0;
			let resQuery = `select q_no from result where teamname="${req.user.username}" order by id desc limit 1`;
				db.query(resQuery,(err3,result3) => {
		
					// console.log(result3);
					if(!result3.length){
						var sql3='INSERT INTO result SET ?';
						var resultstr ={
							teamname: req.user.username,
							q_no:  result[0].id,
							time: DateTime.local().setZone('Asia/Kolkata'),
							startTime: DateTime.local().setZone('Asia/Kolkata')
						}
						db.query(sql3,resultstr,(err4,result4) => {
							if(err4){
								console.log("not tracking in result");
							}else{
								console.log("result4:",result4);
								console.log("tracked in result");
							}
						});
					}
					else{
						count = result3[0].q_no;
						if(result[0].id>count){

							var sql3=`UPDATE result SET ? WHERE q_no=${count} AND teamname="${req.user.username}"`;
							var resultstr ={
								teamname: req.user.username,
								q_no:  result[0].id,
								time: DateTime.local().setZone('Asia/Kolkata')
							}
							db.query(sql3,resultstr,(err4,result4) => {
								if(err4){
									console.log("not tracking in result");
								}else{
									console.log(req.user.username);
									console.log("tracked in result");
								}
							});
						}
					}	
			   })
			//-----------------------

        }
    })
});

module.exports = router;

function ensureAuthenticated(req, res, next){
	if(req.isAuthenticated()){
		return next();
	} else {
		req.flash('error','Please sign in to continue');
		res.redirect('/iohunt18/user/login');
	}
}
