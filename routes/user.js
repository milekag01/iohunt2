var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mysql = require('mysql');
var csurf = require('csurf');

const db=mysql.createConnection({
	host:'localhost',
	user:'root',
	password:'qwerty123',
	port:3306,
	database:'iohuntsql3'
});

db.connect((err) => {
	if(err){
		console.log("error while connecting to db");
	}else{
		console.log('mysql connected');
	}
});

var csrfProtection = csurf();
router.use(csrfProtection);

// if user try to visit /iohunt18/user/
router.get('/', function (req, res) {
	res.redirect('/iohunt18/user/login');
});
// Register
router.get('/signup',csrfProtection, function (req, res) {
	res.render('./user/signup',{csrfToken:req.csrfToken()});
});

// Login
router.get('/login',csrfProtection, function (req, res) {
	res.render('./user/login',{csrfToken:req.csrfToken()});
});


// Register User
router.post('/signup',(req, res) => {
	let username = req.body.username;
	let p1 = req.body.p1;
	let p2 = req.body.p2;
	let email = req.body.email;
	let password = req.body.password;
	let password2 = req.body.password2;

	// Validation.
	req.checkBody('username', 'Team Name is required').notEmpty();
	req.checkBody('p1', 'Name is required').notEmpty();
	req.checkBody('p2', 'Name is required').notEmpty();
	req.checkBody('email', 'Email is required').notEmpty().isEmail();
	req.checkBody('password', 'Password is required').notEmpty();
	req.checkBody('password2', 'Passwords do not match').equals(req.body.password);
	// console.log(username);
	// console.log(p1);
	// console.log(p2);
	// console.log(email);
	// console.log(password);
	// console.log(password2);
	var error = req.validationErrors();
	console.log(errors);
	if (error) {
		res.render('./user/signup', { error: error[0].msg});
		console.log("error in validation");
	}
	else {
		//checking if email or username is already taken
	
		var query1 = 'SELECT * FROM user WHERE username = ?';
		db.query(query1,[username],function(err,user){
			var query2 = 'SELECT * FROM user WHERE email = ?';
			db.query(query2,[email],function(err,mail){
				if(user.length || mail.length){
					console.log("email or teamname already taken");
					req.flash('error', 'Team name or email is already taken. Try another one');
					res.redirect('/iohunt18/user/signup');
				}else{
					var newUser = {
						username: username,
						p1: p1,
						p2: p2,
						email: email,
						password: password
					};
					console.log(newUser.p2);
					var insertQuery ='INSERT INTO user (username,p1,p2,email,password) VALUES (?,?,?,?,?)';
					db.query(insertQuery,[newUser.username,newUser.p1,newUser.p2,newUser.email,newUser.password],(error,result)=>{
						if(error){
							console.log("error in inserting user");
							throw error;
						}
							console.log("user inserted");
							newUser.id = result.insertId;
							console.log(result.insertId);
							console.log(newUser.id);
							req.flash('success', 'Account created successfully. Please login to continue');
							res.redirect('/iohunt18/user/login');
					})
				}
				
			})
		})
	}
});

passport.use(new LocalStrategy(
	function (username, password, done) {
		var query1 = 'SELECT * FROM user WHERE username = ?';
		db.query(query1,[username],(err,user) => {
			if(err){
				console.log("error in sign-in local strategy");
				throw err;
			} 
			else if(!user.length){
				return done(null,false,{message: 'Unknown User'});
			}
			else{
				if(password === user[0].password){
					return done(null,user[0]);
				}
				else{
					return done(null,false,{message : 'Invalid Password'});
				}
			}
			
		});
	}
));

passport.serializeUser(function (user, done) {
	done(null, user.id);
});

passport.deserializeUser(function (id, done) {
	var query = 'SELECT * FROM user WHERE id = ?';
	db.query(query,[id],(error,rows) => {
		done(error,rows[0]);
	});
});

router.post('/login',
	passport.authenticate('local', { successRedirect: '/iohunt18/user/profile', failureRedirect: '/iohunt18/user/login', failureFlash: true }),
	function (req, res) {
		res.redirect('/iohunt18/user/profile');
});

router.get('/logout', function (req, res) {
	req.logout();

	// req.flash('success', 'You are logged out');
	res.redirect('/iohunt18');
});

module.exports = router;
